﻿

#include <iostream>

class Animal
{
    public:
        virtual void Voice()
        {
            std::cout << "Some sound \n";
        }
};

class Cat : public Animal
{
    public:
        void Voice() override
        {
            std::cout << "Meow \n";
        }
};

class Dog : public Animal
{
    public:
        void Voice() override
        {
            std::cout << "Woof \n";
        }
};

class Fox : public Animal
{
    public:
        void Voice() override
        {
            std::cout << "Some strange sound \n";
        }
};

int Random(int min, int max) {
    return min + rand() % (max - min);
}

int main()
{
    int Length = Random(5, 12);
    Animal* animals = new Animal[Length];
    int rnd = 0;
    Animal * CurrentAnimal;
    

    for (int i = 0; i < Length; i++)
    {
        rnd = Random(0, 3);
        switch (rnd)
        {
        case 0:
            CurrentAnimal = new Cat();
            animals[i] = *CurrentAnimal;
            CurrentAnimal->Voice();
            break;
        case 1:
            CurrentAnimal = new Dog();
            animals[i] = *CurrentAnimal;
            CurrentAnimal->Voice();
            break;
        case 2:
            CurrentAnimal = new Fox();
            animals[i] = *CurrentAnimal;
            CurrentAnimal->Voice();
            break;
        }
    }

    delete[] animals;
    delete CurrentAnimal;
}


